#include <GL/glut.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <iostream>
#include <cmath>

GLfloat angle = 0.0f;
GLfloat angle2 = 0.0f;
// GLfloat angle_x = 0.0f;
// GLfloat angle_z = 0.0f;
int moving, startx, starty;

class Tree
{
public:

	Tree() {}

	static void makecylinder(float height, float Base)
	{
		GLUquadricObj *qobj;
		qobj = gluNewQuadric();
		if (height > 1.5)
		{
			glColor3f(0.415, 0.243, 0.243);
		}
		else
		{
			glColor3f(0.325, 0.549, 0.231);
		}
		glPushMatrix();
		glRotatef(-90, 1.0f, 0.0f, 0.0f);
		gluCylinder(qobj, Base, Base - (0.2 * Base), height, 20, 20);
		glPopMatrix();
	}

	static void maketree(float height, float Base)
	{

		glPushMatrix();
		float angle;
		makecylinder(height, Base);
		glTranslatef(0.0f, height, 0.0f);
		height -= height * 0.2f;
		Base -= Base * 0.3f;

		if (height > 1)
		{
			angle = 22.5f;
			glPushMatrix();
			glRotatef(angle, -0.5f, 0.0f, 0.0f);
			maketree(height, Base);
			glColor3f(0.325, 0.549, 0.231);
			glPopMatrix();
			glPushMatrix();
			glRotatef(angle, 0.5f, 0.0f, 0.866f);
			maketree(height, Base);
			glPopMatrix();
			glPushMatrix();
			glRotatef(angle, 0.5f, 0.0f, -0.866f);
			maketree(height, Base);
			glPopMatrix();
			glPushMatrix();
			glRotatef(angle, 0.0f, 0.0f, 0.0f);
			maketree(height, Base);
			glPopMatrix();
		}
		glPopMatrix();
	}

	static void display(void)
	{
		const double t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
		const double a = t * 90.0;
		srand(GLUT_ELAPSED_TIME);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();

		glRotatef(angle, 0, 1, 0);
		glRotatef(angle2, 0, 1, 0);
		// glMultMatrixf(my_rotation_matrix_x());

		maketree(4.0f, 0.1f);
		glutSwapBuffers();
		glFlush();
	}

} tree;

static void
resize(int width, int height)
{
	const float ar = (float)width / (float)height;

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-12.0, 12.0, -1.0, 20.0, -15.0, 15.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// const GLfloat *my_rotation_matrix_y() {
//     return new GLfloat[16]{
//             cosf(angle3 * 0.02f), 0, sinf(angle3 * 0.02f), 0.0,
//             0.0, 1.0, 0.0, 0.0,
//             -sinf(angle3 * 0.02f), cosf(angle3 * 0.02f), 1.0, 0.0,
//             0.0, 0.0, 0.0, 1.0
//     };
// }

// const GLfloat *my_rotation_matrix_x() {
//     return new GLfloat[16]{
//             1.0, 0.0, 0.0, 0.0,
//             0.0, cosf(angle3 * 0.02f), -sinf(angle3 * 0.2f), 0.0,
//             0.0, sinf(angle3 * 0.02f), cosf(angle3 * 0.02f), 0.0
//     };
// }

// const GLfloat *my_rotation_matrix_z() {
//     return new GLfloat[16]{
//             1, 0, 0, 0.0,
//             0.0, cosf(angle3 * 0.02f), -sinf(angle3 * 0.2f), 0.0,
//             0.0, sinf(angle3 * 0.02f), cosf(angle3 * 0.02f), 0.0,
//     };
// }

static void key(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'q':
		exit(0);
		break;
		// case 'f':
		// 	angle_x += 0.25;
		// 	glutPostRedisplay();
		// 	break;
		// case 'F':
		// 	angle_x -= 0.25;
		// 	glutPostRedisplay();
		// 	break;
	}
}

void mouse(int btn, int state, int x, int y)
{
	if (btn == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		moving = 1;
		startx = x;
		starty = y;
	}
	if (btn == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		moving = 0;
	}
}

void motion(int x, int y)
{
	if (moving)
	{
		angle = angle + (x - startx);
		angle2 = angle2 + (y - starty);
		startx = x;
		starty = y;
		glutPostRedisplay();
	}
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitWindowSize(1280, 960);
	glutInitWindowPosition(10, 10);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

	glutCreateWindow("3d fractal tree");

	glutReshapeFunc(resize);
	glutDisplayFunc(tree.display);
	glutKeyboardFunc(key);

	glutMouseFunc(mouse);
	glutMotionFunc(motion);

	glClearColor(0.705, 0.886, 0.952, 1);

	glEnable(GL_DEPTH_TEST);

	glutMainLoop();

	return 0;
}
